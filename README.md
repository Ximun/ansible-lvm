This role is used to construct en entire crypted lvm physical volume. The goal
is to manage partitions also.

### Notes

Encryption and LVM will split into two distincts roles in the future...

## Role parameters

| name                | value   | optionnal  | default_value  | description                                      |
| --------------------|---------|------------|----------------|--------------------------------------------------|
| device.main         | string  | No         | N/A            | The main device to create lvm volume             |
| device.part         | string  | No         | N/A            | Main encrypted part, on which create lvm volume  |
| device.name         | string  | No         | N/A            | Mapper name for the decrypted part               |
| device.keyfile      | string  | No         | N/A            | The LUKS key                                     |
| device.keyfile_path | string  | No         | N/A            | Remote path to keyfile                           |
| lvm.group_name      | string  | No         | N/A            | Name for the LVM Group                           |
| lvm.lv_params       | list    | No         | []             | Configuration for each Logical Volume            |

## Exemple

```yaml
- hosts: all
  roles:
    - role: ansible-lvm
      device:
        main: /dev/sda
        part: 1
        name: system
        keyfile: "{{ keyfile }}"
        keyfile_path: /path/to/keyfile
      lvm:
        group_name: group
        lv_params:
         - { name: root, size: 10G, fstype: ext4 }
         - { name: srv, size: 30G, fstype: ext4 }
         - { name: home, size: 100%FREE, fstype: ext4 }
```
